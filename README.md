# Text Clock #

This is a simple text clock version. It run on *python 3*
It will display only one letter at a time so that it can be used with a led matrice (electronics).
This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###
simply run application.py in a shell


```
#!bash
python application.py

```

### Note ###

The time is displayed in french. I'm planning on making the screen more modular to support multi languages.