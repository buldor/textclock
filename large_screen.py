# encoding: utf-8

WORDS = [
   'il',
	'9',
   'est',
	'8',
   'minuit',
   'midi',
   'deux',
   'vingt',
   'seize',
   'et',
   'cinq',
   'trois',
   'six',
   'quatre',
   'treize',
   'dix',
   'une',
   'huit',
	'3',
   'neuf',
   'onze',
   'quatorze',
   'sept',
   'y',
   'b',
   'quinze',
   'j',
   'k',
   '•',
   'heure',
   '•',
   'w',
	'5',
   'trente',
	'2',
   'moins',
	'0',
   'vingt',
	'4',
	'cinquante',
   'onze',
	'quarante',
   'trois',
   'seize',
   'treize',
	'1',
   'cinq',
   'quatorze',
	'6',
	'7',
   'deux',
   'une',
   'sept',
   'six',
   'quatre',
   'dix',
   'douze',
   'huit',
   'quinze',
   'neuf',
   'le',
   'demi',
   'quart',
]
LETTERS = ''.join(WORDS)

HOURS = {
	0: list((0, x) for x in (0x7, 0x8, 0x9, 0xA, 0xB, 0xC)),
	1: list((2, x) for x in (0x12, 0x13, 0x14)),
	2: list((0, x) for x in (0x11, 0x12, 0x13, 0x14)),
	3: list((1, x) for x in (0x10, 0x11, 0x12, 0x13, 0x14)),
	4: list((2, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4, 0x5)),
	5: list((1, x) for x in (0xC, 0xD, 0xE, 0xF)),
	6: list((2, x) for x in (0x6, 0x7, 0x8)),
	7: list((4, x) for x in (0x0, 0x1, 0x2, 0x3)),
	8: list((3, x) for x in (0x0, 0x1, 0x2, 0x3)),
	9: list((3, x) for x in (0x5, 0x6, 0x7, 0x8)),
	10: list((2, x) for x in (0x9, 0xA, 0xB)),
	11: list((3, x) for x in (0x9, 0xA, 0xB, 0xC)),
	#12: list((1, x) for x in (0x5, 0x6, 0x7, 0x8, 0x9)),  # 12
	12: list((0, x) for x in (0xD, 0xE, 0xF, 0x10)),  # midi
	13: list((2, x) for x in (0xC, 0xB, 0xD, 0xE, 0xF, 0x10)),
	14: list((3, x) for x in (0xD, 0xE, 0xF, 0x10, 0x11, 0x12, 0x13, 0x14)),
	15: list((4, x) for x in (0x4, 0x5, 0x6, 0x7, 0x8, 0x9)),
	16: list((4, x) for x in (0xA, 0xB, 0xC, 0xD, 0xE)),
	20: list((2, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4)),
}
HOURS[17] = HOURS[10] + HOURS[7]
HOURS[18] = HOURS[10] + HOURS[8]
HOURS[19] = HOURS[10] + HOURS[9]
HOURS[21] = HOURS[20] + [(1, 0xA), (1, 0xB)] + HOURS[1]
HOURS[22] = HOURS[20] + HOURS[2]
HOURS[23] = HOURS[20] + HOURS[3]

MINUTES = {
	'et': [(6, 0x8), (6, 0x9)],
	'le': [(9, 0xA), (9, 0xB)],
	'quart': list((9, x) for x in (0x10, 0x11, 0x12, 0x13, 0x14)),
	'demi': list((9, x) for x in (0xC, 0xD, 0xE, 0xF)),
	'moins': list((5, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4)),
	0: list(),
	1: list((6, x) for x in (0xE, 0xF, 0x10)),
	2: list((6, x) for x in (0xA, 0xB, 0xC, 0xD)),
	3: list((8, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4)),
	4: list((7, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4, 0x5)),
	5: list((8, x) for x in (0x11, 0x12, 0x13, 0x14)),
	6: list((7, x) for x in (0x6, 0x7, 0x8)),
	7: list((7, x) for x in (0x11, 0x12, 0x13, 0x14)),
	8: list((8, x) for x in (0x11, 0x12, 0x13, 0x14)),
	9: list((9, x) for x in (0x6, 0x7, 0x8, 0x9)),
	10: list((7, x) for x in (0x9, 0xA, 0xB)),
	11: list((5, x) for x in (0x11, 0x12, 0x13, 0x14)),
	12: list((7, x) for x in (0xC, 0xD, 0xE, 0xF, 0x10)),
	13: list((8, x) for x in (0xA, 0xB, 0xC, 0xD, 0xE)),
	14: list((6, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7)),
	15: list((9, x) for x in (0x0, 0x1, 0x2, 0x3, 0x4, 0x5)),
	16: list((8, x) for x in (0x5, 0x6, 0x7, 0x8, 0x9)),
	20: list((5, x) for x in (0x6, 0x7, 0x8, 0x9, 0xA)),
	30: list((5, x) for x in (0xB, 0xC, 0xD, 0xE, 0xF, 0x10)),
}
MINUTES[17] = MINUTES[10] + MINUTES[7]
MINUTES[18] = MINUTES[10] + MINUTES[8]
MINUTES[19] = MINUTES[10] + MINUTES[9]
MINUTES[21] = MINUTES[20] + MINUTES['et'] +  MINUTES[1]
MINUTES[22] = MINUTES[20] + MINUTES[2]
MINUTES[23] = MINUTES[20] + MINUTES[3]
MINUTES[24] = MINUTES[20] + MINUTES[4]
MINUTES[25] = MINUTES[20] + MINUTES[5]
MINUTES[26] = MINUTES[20] + MINUTES[6]
MINUTES[27] = MINUTES[20] + MINUTES[7]
MINUTES[28] = MINUTES[20] + MINUTES[8]
MINUTES[29] = MINUTES[20] + MINUTES[9]
MINUTES[31] = MINUTES[30] + MINUTES['et'] +  MINUTES[1]
MINUTES[32] = MINUTES[30] + MINUTES[2]
MINUTES[33] = MINUTES[30] + MINUTES[3]
MINUTES[34] = MINUTES[30] + MINUTES[4]
MINUTES[35] = MINUTES[30] + MINUTES[5]
MINUTES[36] = MINUTES[30] + MINUTES[6]
MINUTES[37] = MINUTES[30] + MINUTES[7]
MINUTES[38] = MINUTES[30] + MINUTES[8]
MINUTES[39] = MINUTES[30] + MINUTES[9]
MINUTES[40] = MINUTES['moins'] + MINUTES[20]
MINUTES[41] = MINUTES['moins'] + MINUTES[19]
MINUTES[42] = MINUTES['moins'] + MINUTES[18]
MINUTES[43] = MINUTES['moins'] + MINUTES[17]
MINUTES[44] = MINUTES['moins'] + MINUTES[16]
MINUTES[45] = MINUTES['moins'] + MINUTES['le'] + MINUTES['quart']
MINUTES[46] = MINUTES['moins'] + MINUTES[14]
MINUTES[47] = MINUTES['moins'] + MINUTES[13]
MINUTES[48] = MINUTES['moins'] + MINUTES[12]
MINUTES[49] = MINUTES['moins'] + MINUTES[11]
MINUTES[50] = MINUTES['moins'] + MINUTES[10]
MINUTES[51] = MINUTES['moins'] + MINUTES[9]
MINUTES[52] = MINUTES['moins'] + MINUTES[8]
MINUTES[53] = MINUTES['moins'] + MINUTES[7]
MINUTES[54] = MINUTES['moins'] + MINUTES[6]
MINUTES[55] = MINUTES['moins'] + MINUTES[5]
MINUTES[56] = MINUTES['moins'] + MINUTES[4]
MINUTES[57] = MINUTES['moins'] + MINUTES[3]
MINUTES[58] = MINUTES['moins'] + MINUTES[2]
MINUTES[59] = MINUTES['moins'] + MINUTES[1]


SCREEN = [LETTERS[l:l+21].upper() for l in range(0, 231, 21)]
SCREEN2 = [LETTERS[l:l+42].upper() for l in range(0, 231, 42)]

