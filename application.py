from datetime import datetime, timedelta

from screen import SCREEN, HOURS, MINUTES, IL_EST, clear_screen


def light_coordinate(point_row, point_column):
    """
    will display a blank screen except at the given coordinate
    """
    for y, column in enumerate(SCREEN):
        selected_column = y == point_column
        for x, row in enumerate(column):
            if selected_column and x == point_row:
                print(SCREEN[point_column][point_row], end='')
            else:
                print(' ', end='')
        print()


def time_coordinates(the_time=None):
    """
    returns a list of coordinates to display the time given as argument.
    If no time is given then, the current time will be used
    """
    the_time = the_time or datetime.now()

    # round minutes
    displayed_minutes = int(the_time.minute / 15)

    # add one hour if displayed minutes is "a quarter to"
    if displayed_minutes == 3:
        the_time = the_time + timedelta(hours=1)

    # change to am hour except for noon
    displayed_hour = the_time.hour % 12 if the_time.hour != 12 else the_time.hour

    # finally returns the points
    return IL_EST + HOURS[displayed_hour] + MINUTES[displayed_minutes]


if __name__ == '__main__':
    while True:
        for point in time_coordinates():
            clear_screen(0.2)
            light_coordinate(*point)
