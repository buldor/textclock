from time import sleep

#          012345678901
SCREEN = ['IL4EST267985',  # 0
          '110423MINUIT',  # 1
          'HEURES42MIDI',  # 2
          '124MOINSETLE',  # 3
          '86DEMIEQUART']  # 4

IL_EST = [(0, 0), (1, 0), (3, 0), (4, 0), (5, 0)]

HEURE = [(0, 2), (1, 2), (2, 2), (3, 2), (4, 2)]
HEURES = HEURE + [(5, 2)]

HOURS = {
    0: [(6, 1), (7, 1), (8, 1), (9, 1), (10, 1), (11, 1)],  # minuit
    1: [(1, 1)] + HEURE,
    2: [(4, 1)] + HEURES,
    3: [(5, 1)] + HEURES,
    4: [(3, 1)] + HEURES,
    5: [(11, 0)] + HEURES,
    6: [(7, 0)] + HEURES,
    7: [(8, 0)] + HEURES,
    8: [(10, 0)] + HEURES,
    9: [(9, 0)] + HEURES,
    10: [(1, 1), (2, 1)] + HEURES,
    11: [(0, 1), (1, 1)] + HEURES,
    12: [(8, 2), (9, 2), (10, 2), (11, 2)],  # midi
}

MINUTES = {
    0: [],
    1: [(8, 3), (9, 3), (7, 4), (8, 4), (9, 4), (10, 4), (11, 4)],  # et quart
    2: [(8, 3), (9, 3), (2, 4), (3, 4), (4, 4), (5, 4), (6, 4)],  # et demie
    3: [(3, 3), (4, 3), (5, 3), (6, 3), (7, 3), (10, 3), (11, 3), (7, 4), (8, 4), (9, 4), (10, 4), (11, 4)],
    # moins le quart
}


def clear_screen(pause=None):
    if pause:
        sleep(pause)
    print('\x1b[2J\x1b[H')

